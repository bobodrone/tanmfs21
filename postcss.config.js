/* eslint-disable import/no-extraneous-dependencies, global-require */
const plugins = [require('tailwindcss'), require('autoprefixer')];

module.exports = { plugins };
