module.exports = {
  purge: {
    layers: ['components', 'utilities'],
    content: ['./src/**/*.njk', './src/**/*.md']
  },
  theme: {
    extend: {
      fontFamily: {
        'sans': ['"Fjalla One"', 'ui-sans-serif']
      },
      backgroundImage: (theme) => ({
        "header-bg2": "url('assets/img/white.jpg')",
      }),
      colors: {
        grayish: "#cccccc",
      }
    }
  },
  variants: {
    aspectRatio: ['responsive', 'hover'],
    extend: {
      padding: ['first', 'last'],
    }
  },
  plugins: [
    require('@tailwindcss/aspect-ratio')
  ],
};
