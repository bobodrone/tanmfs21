---
layout: layouts/one-col.njk
title: Bios
templateClass: page
eleventyNavigation:
  key: Bios
  order: "3"
  icon: text
---
![Mattias Petersson](/assets/img/matt.jpg)<small>Photo: Marcus Wrangö</small>

**Mattias Petersson** was born in 1972 on an island off the southeast coast of Sweden. His musical career began with piano studies but nowadays he works as a composer and plays electronic instruments. He works mostly within the realm of experimental electronic music and sound art, but he has also been involved in more pop-like projects as an arranger, composer, producer and musician.
### More about Mattias Petersson

* [http://www.mattiaspetersson.com](http://www.mattiaspetersson.com)

---

![George Kentros](/assets/img/george.jpg)<small>Photo: Anonymous</small>

**George Kentros** was born and raised in the US but now lives in Sweden. He has performed as a chamber musician, soloist, and sometime actor across Europe as well as in the US, Central America, and Japan, primarily with the new music ensemble "the peärls before swïne experience." When not practicing scales, writing theater music, or touring, he produces a semi-weekly avant-garde club in Stockholm.

### More about George Kentros

* [https://www.georgekentros.com](https://www.georgekentros.com)
* [http://www.swinepearl.com](http://www.swinepearl.com)
