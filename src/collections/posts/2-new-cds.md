---
title: Two CDs out!
description: We have released two new cds.
date: 2020-05-01
tags:
  - cds
  - vivaldi
  - biber
layout: layouts/post.njk
---

**[Buy and read more here!](https://nm4s.bandcamp.com/album/there-are-no-more-four-seasons)**

The best classical album of 2013 according to **Martin Nyström** *(Dagens Nyheter)*

The followup to the legendary self-titled debut CD by there are no more four seasons
Released by SEKT records
On Bandcamp