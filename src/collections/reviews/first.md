---
title: DN - Martin Nyström
author: Martin Nyström
date: 2020-12-27
magazine: Dagens Nyheter
excerpt: A high-speed journey into a center of pure musical energy...
excerpt_se: En höghastighetsresa in i ett centrum av ren musikalisk energi...
layout: layouts/review.njk
permalink: reviews/{{ title | slug }}/index.html

---
A high-speed journey into a center of pure musical energy.