---
title: Perhaps the most interesting Swedish album of the year
author: Claes Olsson
date: 2020-12-25
magazine: Musikindustri.se
excerpt: Perhaps the most interesting Swedish album of the year.
excerpt_se: Troligen det absolut mest intressanta Svenska albumet det här året.
layout: layouts/review.njk
permalink: reviews/{{ title | slug }}/index.html

---
Perhaps the most interesting Swedish album of the year.