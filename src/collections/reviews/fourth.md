---
title: DN - Johanna Paulsson
author: Johanna Paulsson
date: 2020-12-26
magazine: Dagens Nyheter
excerpt: "...from the ashes, something wholly new is created. It's as if the melancholy
  violin rises from the ruins and, like a postapocalyptic Mad Max, dusts himself off
  to continue towards new adventures. At that moment I refuse to believe all the predictions
  that classical music is withering away."
excerpt_se: "...från askan skapas något helt nytt. Det är som om den melankoliska
   fiolen stiger upp från ruinerna och som en postapokalyptisk Mad Max dammar den av sig själv
   att fortsätta mot nya äventyr. I det ögonblicket vägrar jag att tro på alla förutsägelser
   att klassisk musik vissnar bort."
layout: layouts/review.njk
permalink: reviews/{{ title | slug }}/index.html

---
...from the ashes, something wholly new is created. It's as if the melancholy violin rises from the ruins and, like a postapocalyptic Mad Max, dusts himself off to continue towards new adventures. At that moment I refuse to believe all the predictions that classical music is withering away.