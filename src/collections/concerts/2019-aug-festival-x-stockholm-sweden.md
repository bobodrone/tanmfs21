---
title: ReBiber @ GAS-festival 2019
date: 2019-08-27
context: GAS-festival 2019
location: Stockholm, Sweden
venue: Masthuggstorget
programme: ReBiber, Bergström - "Trial 'n' Error"
excerpt: A high-speed journey into a center of pure musical energy...
layout: layouts/review.njk
permalink: concerts/{{ title | slug }}/index.html

---
A high-speed journey into a center of pure musical energy.