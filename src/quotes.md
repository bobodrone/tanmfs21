<h2>Reviews & Comments:</h2>

<blockquote>
    "The best classical album of 2013" According to <strong>Martin Nyström</strong> (Dagens Nyheter)
</blockquote>

<blockquote>
    "Perhaps the most interesting Swedish album of the year" <strong>Claes Olsson</strong>, Musikindustri.se
</blockquote>

<blockquote>
    "..from the ashes, something wholly new is created. It's as if the melancholy violin rises from the ruins and, like a postapocalyptic Mad Max, dusts himself off to continue towards new adventures. At that moment I refuse to believe all the predictions that classical music is withering away." <strong>Johanna Paulsson</strong>, DN
</blockquote>

<blockquote>
    "a high-speed journey into a center of pure musical energy" <strong>Martin Nyström</strong>, DN
</blockquote>

More reviews can be found <a href="/press">here</a>
